const kBarCodeData = '10906KF427741079473';
const kLeftQrCodeData =
    'AB112233441020523999900000145000001540000000001234567ydXZt4LAN1UHN/j1juVcRA==:**********:3:3:1:乾電池:1:105:';
const kRightQrCodeData = '**口罩:1:210:牛奶:1:25';
const kLeftQrCodeDataBase64 =
    'AB112233441020523999900000145000001540000000001234567ydXZt4LAN1UHN/j1juVcRA==:**********:3:3:2:5Lm+6Zu75rGg';
const kRightQrCodeDataBase64 = '**OjE6MTA1OuWPo+e9qToxOjIxMDrniZvlpbY6MToyNQ==';
