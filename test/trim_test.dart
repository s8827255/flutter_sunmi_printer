import 'package:flutter_test/flutter_test.dart';

main() {
  group('trim', () {
    final testString1 = '\naa bb cc  ';
    final testString2 = '\naa bb\ncc  ';
    test('trim1', () {
      final trimString = testString1.trim();
      expect(trimString, 'aa bb cc');
    });

    test('trim2', () {
      final trimString = testString2.trim();
      expect(trimString, 'aa bb\ncc');
    });
  });
}
