import 'dart:convert';

import 'package:flutter_sunmi_printer/flutter_sunmi_printer.dart';
import 'package:flutter_test/flutter_test.dart';

main() {
  group('encrypt', () {
    const decodedString = ':1:105:口罩:1:210:牛奶:1:25';
    const encodedString = 'OjE6MTA1OuWPo+e9qToxOjIxMDrniZvlpbY6MToyNQ==';
    test('aes', () {
      final actual = SunmiUtil.getEncryptedString(
        aesKey: '6647A889B4B5912BECB5D01065CCD670', // official test
        // aesKey: kAESKey,
        invoiceNumber: 'AA12345678',
        randomNumber: '1234',
      );
      expect(actual, 'dSypnr83S3oOPU5HiEx49w==');
    });

    test('base64Encode', () {
      final bytes = utf8.encode(decodedString);
      expect(base64Encode(bytes), encodedString);
    });

    test('base64Decode', () {
      final bytes = base64Decode(encodedString);
      expect(utf8.decode(bytes), decodedString);
    });
  });
}
