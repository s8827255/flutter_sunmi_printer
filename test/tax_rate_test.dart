import 'package:flutter_sunmi_printer/flutter_sunmi_printer.dart';
import 'package:flutter_test/flutter_test.dart';

main() {
  // 折價品項
  final itemDiscount = Item(
    itemName: '現場折價',
    quantity: 1,
    unitPrice: -90.0,
    taxRate: 0.05,
    taxType: 1,
  );
  // 免稅品項
  final itemFreeTax = Item(
    itemName: '雞蛋',
    quantity: 1,
    unitPrice: 140.0,
    taxRate: 0.0,
    taxType: 3,
  );
  // 應稅品項
  final itemTXTax = Item(
    itemName: '可樂',
    quantity: 1,
    unitPrice: 100.0,
    taxRate: 0.05,
    taxType: 1,
  );

  final invoice = Invoice();

  group('混合稅率', () {
    test('item free', () {
      expect(itemFreeTax.taxAmount.round(), 0);
      expect(itemFreeTax.salesAmount.round(), 0);
      expect(itemFreeTax.freeSalesAmount.round(), 140);
      expect(itemFreeTax.totalAmount.round(), 140);
    });

    test('item TX', () {
      expect(itemTXTax.taxAmount.round(), 5);
      expect(itemTXTax.salesAmount.round(), 95);
      expect(itemTXTax.freeSalesAmount.round(), 0);
      expect(itemTXTax.totalAmount.round(), 100);
    });

    test('item discount', () {
      expect(itemDiscount.taxAmount.round(), 0);
      expect(itemDiscount.salesAmount.round(), 0);
      expect(itemDiscount.freeSalesAmount.round(), 0);
      expect(itemDiscount.totalAmount.round(), -90);
    });
  });

  group('invoice', () {
    test('all without buyer', () {
      invoice.buyer = '';
      invoice.items = [itemDiscount, itemFreeTax, itemTXTax];
      invoice.refresh();
      expect(invoice.hasBuyer, false);
      expect(invoice.taxType, TaxRate.Mix.value);
      expect(invoice.itemsFreeSalesAmount.round(), 140);
      expect(invoice.displayItemsTXSalesAmount, '95元');
      expect(invoice.displayItemsSalesAmount, '235元');
      expect(invoice.displayItemsTaxAmount, '5元');
      expect(invoice.displayItemsTotalAmount, '150元');
      // 直接取值，無運算
      // expect(invoice.displayTotalAmount, '總計 100');
    });

    test('all with buyer', () {
      invoice.buyer = '83193989';
      invoice.items = [itemDiscount, itemFreeTax, itemTXTax];
      invoice.refresh();
      expect(invoice.hasBuyer, true);
      expect(invoice.taxType, TaxRate.Mix.value);
      expect(invoice.displayItemsFreeSalesAmount, '140元');
      expect(invoice.displayItemsTXSalesAmount, '95元');
      expect(invoice.displayItemsSalesAmount, '235元');
      expect(invoice.displayItemsTaxAmount, '5元');
      expect(invoice.displayItemsTotalAmount, '150元');
    });

    test('mix', () {
      invoice.buyer = '83193989';
      invoice.items = [itemFreeTax, itemTXTax];
      invoice.refresh();
      expect(invoice.hasBuyer, true);
      expect(invoice.taxType, TaxRate.Mix.value);
      expect(invoice.displayItemsFreeSalesAmount, '140元');
      expect(invoice.displayItemsTXSalesAmount, '95元');
      expect(invoice.displayItemsSalesAmount, '235元');
      expect(invoice.displayItemsTaxAmount, '5元');
      expect(invoice.displayItemsTotalAmount, '240元');
    });

    test('TX with buyer', () {
      invoice.buyer = '83193989';
      invoice.items = [itemTXTax];
      invoice.refresh();
      expect(invoice.hasBuyer, true);
      expect(invoice.taxType, TaxRate.TX.value);
      expect(invoice.displayItemsFreeSalesAmount, '0元');
      expect(invoice.displayItemsTXSalesAmount, '95元');
      expect(invoice.displayItemsSalesAmount, '95元');
      expect(invoice.displayItemsTaxAmount, '5元');
      expect(invoice.displayItemsTotalAmount, '100元');
    });

    test('TX without buyer', () {
      invoice.buyer = '';
      invoice.items = [itemTXTax];
      invoice.refresh();
      expect(invoice.hasBuyer, false);
      expect(invoice.taxType, TaxRate.TX.value);
      expect(invoice.displayItemsTaxAmount, '0元');
      expect(invoice.displayItemsFreeSalesAmount, '0元');
      expect(invoice.displayItemsTXSalesAmount, '100元');
      expect(invoice.displayItemsSalesAmount, '100元');
      expect(invoice.displayItemsTotalAmount, '100元');
    });

    test('Free', () {
      invoice.buyer = '83193989';
      invoice.items = [itemFreeTax];
      invoice.refresh();
      expect(invoice.hasBuyer, true);
      expect(invoice.taxType, TaxRate.Free.value);
      expect(invoice.displayItemsFreeSalesAmount, '140元');
      expect(invoice.displayItemsTXSalesAmount, '0元');
      expect(invoice.displayItemsSalesAmount, '140元');
      expect(invoice.displayItemsTaxAmount, '0元');
      expect(invoice.displayItemsTotalAmount, '140元');
    });

    test('Discount', () {
      invoice.buyer = '83193989';
      invoice.items = [itemDiscount];
      invoice.refresh();
      expect(invoice.hasBuyer, true);
      expect(invoice.taxType, TaxRate.TX.value);
      expect(invoice.displayItemsFreeSalesAmount, '0元');
      expect(invoice.displayItemsTXSalesAmount, '0元');
      expect(invoice.displayItemsSalesAmount, '0元');
      expect(invoice.displayItemsTaxAmount, '0元');
      expect(invoice.displayItemsTotalAmount, '0元');
    });
  });
}
