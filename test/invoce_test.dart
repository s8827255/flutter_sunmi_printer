import 'package:flutter_sunmi_printer/flutter_sunmi_printer.dart';
import 'package:flutter_test/flutter_test.dart';

main() {
  group('invoice', () {
    final invoiceNumber = 'AA12345678';
    final randomNumber = '1234';
    final seller = '83193989';
    final buyer = '12345678';
    final encrypted = 'vt2qXrGN7o/Xi2r5W1uADw==';
    final base64Item = '5ZWG5ZOB5LiA5om5OjE6MTAwMA==';
    final taxRate = 0.05;
    final customField = ''.padRight(10, '*');
    final invoice = Invoice(
      invoiceNumber: invoiceNumber,
      randomNumber: randomNumber,
      seller: seller,
      buyer: buyer,
      dateTime: DateTime(2017, 12, 1, 18, 32),
      totalAmount: 1000.0,
      taxRate: taxRate,
      items: [
        Item(
          itemName: '商品一批',
          quantity: 1,
          unitPrice: 1000,
        ),
      ],
    );

    test('length', () {
      final fixedString =
          '${invoiceNumber}1061201${randomNumber}000003B8000003E8$buyer$seller$encrypted';
      expect(invoice.fixedString, fixedString);
      expect(invoice.fixedString.length, kLeftStringFixedLength);
    });

    test('barcode', () {
      expect(invoice.barcode, '10612$invoiceNumber$randomNumber');
    });

    test('left string', () {
      final actual = invoice.leftString;
      expect(actual,
          '${invoiceNumber}1061201${randomNumber}000003B8000003E8$buyer$seller$encrypted:$customField:1:1:2:$base64Item');
    });

    test('right string', () {
      final actual = invoice.rightString;
      expect(actual, '**');
    });

    test('item length', () {
      final count = invoice.getMaxItemsCount(kRemainLength);
      final actual = invoice.getItemBase64String(0, count);
      expect(actual.length <= kRemainLength, true);
    });

    test('item', () {
      final count = invoice.getMaxItemsCount(kRemainLength);
      final actual = invoice.getItemBase64String(0, count);
      expect(actual, base64Item);
    });

    test('carrier id', () {
      expect(SunmiUtil.isCarrierId('/KA-+.01'), true);
      expect(SunmiUtil.isCarrierId('/kA-+.01'), false);
      expect(SunmiUtil.isCarrierId('/KA-+.0'), false);
      expect(SunmiUtil.isCarrierId('/KA-*.01'), false);
    });

    test('npo ban', () {
      expect(SunmiUtil.isNpoBan('000'), true);
      expect(SunmiUtil.isNpoBan('1234567'), true);
      expect(SunmiUtil.isNpoBan('01'), false);
      expect(SunmiUtil.isNpoBan('E234567'), false);
    });
  });
}
