import 'dart:math';

import 'package:flutter_sunmi_printer/flutter_sunmi_printer.dart';
import 'package:intl/intl.dart';

import 'constants.dart';
import 'src/enums.dart';
import 'src/invoice.dart';
import 'src/receipt.dart';

extension ExtensionInvoice on Invoice {
  void refresh() {
    // final types = this.items.map((e) => e.taxType).toSet();
    // if (types.length > 1) {
    //   this.taxType = 9;
    // } else {
    //   this.taxType = types.first;
    // }
    if (this.itemsTXSalesAmount > 0 && this.itemsFreeSalesAmount > 0) {
      this.taxType = TaxRate.Mix.value;
    } else {
      if (this.itemsFreeSalesAmount > 0) {
        this.taxType = TaxRate.Free.value;
      } else {
        this.taxType = TaxRate.TX.value;
      }
    }
    this.items.forEach((element) {
      element.taxRate = this.showTax ? 0.05 : 0.0;
    });
  }

  bool get isTaxTypeMix {
    return 9 == this.taxType;
  }

  bool get showTax {
    // HACK:
    // return true;
    return this.hasBuyer || this.isTaxTypeMix;
  }

  bool get hasBuyer {
    // HACK:
    // return true;
    return buyer?.isNotEmpty ?? false;
  }

  bool get isPrinted {
    // HACK:
    // return true;
    this.printMark ??= 0;
    return this.printMark != 0;
  }

  set isPrinted(bool value) {
    this.printMark = value ? 1 : 0;
  }

  String get displayPrintMark {
    final mark = this.isPrinted ? '補印' : '';
    return '電子發票證明聯$mark';
  }

  SunmiSize get printMarkSize {
    return this.isPrinted ? SunmiSize.xl : SunmiSize.xxl;
  }

  String get _twYear {
    if (this.dateTime == null) {
      return '';
    }
    return '${this.dateTime.year - 1911}';
  }

  String get _month {
    if (this.dateTime == null) {
      return '';
    }
    return '${this.dateTime.month}'.padLeft(2, '0');
  }

  String get _day {
    if (this.dateTime == null) {
      return '';
    }
    return '${this.dateTime.day}'.padLeft(2, '0');
  }

  String get displayTwDateTime {
    if (this.dateTime == null) {
      return '';
    }
    final twYear = this._twYear;
    final currMonthString = this._month;
    if (this.dateTime.month % 2 > 0) {
      final nextMonth = this.dateTime.month + 1;
      final nextMonthString = '$nextMonth'.padLeft(2, '0');
      return '$twYear年$currMonthString-$nextMonthString月';
    } else {
      final preMonth = this.dateTime.month - 1;
      final preMonthString = '$preMonth'.padLeft(2, '0');
      return '$twYear年$preMonthString-$currMonthString月';
    }
  }

  String get displayInvoiceNumber {
    this.invoiceNumber ??= '';
    // AB
    final regexpA = RegExp(r'^[A-Z]{2}');
    final matchA = regexpA.firstMatch(this.invoiceNumber);
    final newTextA = matchA?.group(0) ?? '';
    // 12345678
    final regexp0 = RegExp(r'[0-9]{1,8}');
    final match0 = regexp0.firstMatch(this.invoiceNumber);
    final newText0 = match0?.group(0) ?? '';
    //
    final newText = '$newTextA-$newText0';
    return newText;
  }

  String get displayDate {
    if (this.dateTime == null) {
      return '';
    }
    return DateFormat("yyyy-MM-dd").format(this.dateTime);
  }

  String get displayDateTime {
    if (this.dateTime == null) {
      return '';
    }
    return DateFormat("yyyy-MM-dd HH:mm:ss").format(this.dateTime);
  }

  String get displayTaxType {
    return this.hasBuyer ? '格式25' : '';
  }

  String get displayRandomNumber {
    this.randomNumber ??= '';
    return '隨機碼 ${this.randomNumber}';
  }

  String get displayTotalAmount {
    this.totalAmount ??= 0.0;
    return '總計 ${this.totalAmount.currency}';
  }

  String get displayBuyer {
    return this.hasBuyer ? '買方 ${this.buyer}' : '';
  }

  String get displaySeller {
    return '賣方 ${this.seller}';
  }

  String get barcode {
    if (this.dateTime == null) {
      return '';
    }
    final value1 = this._twYear;
    final mm = this.dateTime.month;
    final month = mm % 2 > 0 ? mm + 1 : mm;
    final value2 = '$month'.padLeft(2, '0');
    final value3 = this.invoiceNumber ?? '';
    final value4 = '${this.randomNumber ?? 0}'.padLeft(4, '0');
    return '$value1$value2$value3$value4';
  }

  String get fixedString {
    final value1 = this.invoiceNumber; // 發票字軌 ZJ71292204
    final value2 = this._twYYYMMdd; // 發票開立日期 1090312
    final value3 = this.randomNumber; // 隨機碼 3612
    final value4 = this._totalHexWithoutTax; // 銷售額 00000055 (85)
    final value5 = this._totalHexWithTax; // 總計額 00000059 (89)
    final value6 = this._buyer; // 買方統一編號 00000000
    final value7 = this.seller; // 賣方統一編號 42308086
    final value8 = this._getEncryptedString(kAESKey); // 加密驗證資訊
    return '$value1$value2$value3$value4$value5$value6$value7$value8';
  }

  String get leftString {
    // final value1 = this.invoiceNumber; // 發票字軌 ZJ71292204
    // final value2 = this._twYYYMMdd; // 發票開立日期 1090312
    // final value3 = this.randomNumber; // 隨機碼 3612
    // final value4 = this._totalHexWithoutTax; // 銷售額 00000055 (85)
    // final value5 = this._totalHexWithTax; // 總計額 00000059 (89)
    // final value6 = this._buyer; // 買方統一編號 00000000
    // final value7 = this.seller; // 賣方統一編號 42308086
    // final value8 = this._getEncryptedString(kAESKey); // 加密驗證資訊
    final value9 = this._customField; // 營業人自行使用區 **********
    final leftCount = this.getMaxItemsCount(kRemainLength);
    final rightCount = this.getMaxItemsCount(kQrCodeLength - 2, leftCount);
    final value10 = leftCount + rightCount; // 二維條碼記載完整品 3
    final value11 = this._itemCount; // 該張發票交易品目總筆數 3
    final value12 = this._stringEncode; // 中文編碼參數 2 (base64)
    final value13 = this.getItemBase64String(0, leftCount);
    //
    final sector1 = this.fixedString;
    final sector2 = ':$value9:$value10:$value11:$value12:$value13';
    return '$sector1$sector2';
  }

  String get leftQrString {
    return leftString.padRight(128, ' ');
  }

  num getMaxItemsCount(num length, [int start = 0]) {
    num count = 0;
    String value1 = '';
    this.items ??= [];
    for (var i = start; i < this.items.length; i++) {
      final item = this.items.elementAt(i);
      final value2 = '$value1:${item.invoiceString}';
      final value3 = SunmiUtil.getBase64String(value2);
      if (value3.length > length) {
        break;
      }
      value1 = value2;
      count++;
    }
    return count;
  }

  // String get _itemString {
  //   final count = this.getMaxItemsCount(kRemainLength);
  //   final value1 = this.items.take(count).fold('',
  //       (previousValue, element) => '$previousValue:${element.invoiceString}');
  //   return SunmiUtil.getBase64String(value1);
  // }

  String getItemBase64String(num start, num end) {
    final value1 = this.items.getRange(start, end).fold<String>(
      '',
      (previousValue, element) {
        if (previousValue.isEmpty) {
          return element.invoiceString;
        }
        return '$previousValue:${element.invoiceString}';
      },
    );
    return SunmiUtil.getBase64String(value1);
  }

  num get _itemCount {
    this.items ??= [];
    return this.items.length;
  }

  String get rightString {
    final value1 = '**';
    final leftCount = this.getMaxItemsCount(kRemainLength);
    final rightCount = this.getMaxItemsCount(kQrCodeLength - 2, leftCount);
    final value2 = rightCount > 0
        ? this.getItemBase64String(leftCount, leftCount + rightCount)
        : '';
    return '$value1$value2';
  }

  String get rightQrString {
    return this.rightString.padRight(128, ' ');
  }

  String get _twYYYMMdd {
    final y = this._twYear;
    final m = this._month;
    final d = this._day;
    return '$y$m$d';
  }

  String get _buyer {
    return (this.buyer ?? '').padLeft(8, '0');
  }

  /// 中文編碼參數 (1 碼)：定義後續資訊的編碼規格
  /// 0: big5
  /// 1: uft8
  /// 2: base64
  String get _stringEncode {
    return '2';
  }

  ///
  /// 營業人自行使用區 (10 碼)：提供營業人自行放置所需資訊，若不使用則以 10 個“*”符號呈現。
  ///
  String get _customField {
    return ''.padLeft(10, '*');
  }

  String _getEncryptedString(String aesKey) {
    return SunmiUtil.getEncryptedString(
      aesKey: aesKey,
      invoiceNumber: this.invoiceNumber,
      randomNumber: this.randomNumber,
    );
  }

  String get _totalHexWithTax {
    this.totalAmount ??= 0.0;
    final price = max(this.totalAmount, 0.0);
    final priceRound = price.round();
    final t = priceRound.toRadixString(16);
    final up = t.toUpperCase();
    return up.padLeft(8, '0');
  }

  String get _totalHexWithoutTax {
    this.totalAmount ??= 0.0;
    this.taxRate ??= 0.0;
    final untaxPrice = max(this.totalAmount, 0.0) / (1.0 + this.taxRate);
    final untaxPriceRound = untaxPrice.round();
    final t = untaxPriceRound.toRadixString(16);
    final up = t.toUpperCase();
    return up.padLeft(8, '0');
  }

  num get itemsTotalAmount {
    return items.fold<num>(
        0.0, (previousValue, element) => previousValue + element.totalAmount);
  }

  String get displayItemsTotalAmount {
    final value1 = max(0.0, itemsTotalAmount);
    final value2 = value1.round().currency;
    return '$value2元';
  }

  String get displayItemsCount {
    final value = items.fold<num>(0, (previousValue, element) {
      element.quantity ??= 0;
      return previousValue + element.quantity;
    });
    return '$value項';
  }

  num get itemsTaxAmount {
    return items.fold<num>(
        0.0, (previousValue, element) => previousValue + element.taxAmount);
  }

  String get displayItemsTaxAmount {
    final value1 = itemsTaxAmount.round().currency;
    return '$value1元';
  }

  num get itemsSalesAmount {
    return items.fold<num>(
        0.0,
        (previousValue, element) =>
            previousValue + element.salesAmount + element.freeSalesAmount);
  }

  String get displayItemsSalesAmount {
    final value1 = itemsSalesAmount.round().currency;
    return '$value1元';
  }

  num get itemsFreeSalesAmount {
    return items.fold<num>(0.0,
        (previousValue, element) => previousValue + element.freeSalesAmount);
  }

  String get displayItemsFreeSalesAmount {
    final value1 = itemsFreeSalesAmount.round().currency;
    return '$value1元';
  }

  num get itemsTXSalesAmount {
    return items.fold<num>(
        0.0, (previousValue, element) => previousValue + element.salesAmount);
  }

  String get displayItemsTXSalesAmount {
    final value1 = itemsTXSalesAmount.round().currency;
    return '$value1元';
  }
}

extension ExtensionItem on Item {
  String get invoiceString {
    this.quantity ??= 0;
    this.unitPrice ??= 0.0;
    final value1 = this.itemName;
    final value2 = this.quantity.round();
    final value3 = this.unitPrice.round();
    return '$value1:$value2:$value3';
  }

  String get displayUnitPrice {
    this.unitPrice ??= 0.0;
    return this.unitPrice.round().currency;
  }

  String get leftString {
    this.quantity ??= 1; // 預設 1
    return '  \$${this.displayUnitPrice} x ${this.quantity}';
  }

  String get rightString {
    this.taxRate ??= 0.0;
    // final value2 = this.taxRate > 0.0 ? ' TX' : '';
    final value2 = 1 == this.taxType ? ' TX' : '';
    return '${this.displayTotal}元$value2';
  }

  String get displayTotal {
    return this.totalAmount.round().currency;
  }

  num get totalAmount {
    this.quantity ??= 0;
    this.unitPrice ??= 0.0;
    return this.quantity * this.unitPrice;
  }

  // num get salesAmountWithType {
  //   this.taxType ??= 1; // 預設應稅
  //   final taxRate = (1 == this.taxType) ? 0.05 : 0.0;
  //   return this.totalAmount / (1.0 + taxRate);
  // }

  // num get taxAmountWithType {
  //   return this.totalAmount - this.salesAmountWithType;
  // }

  num get freeSalesAmount {
    this.taxType ??= 1; // 預設應稅
    if (3 == this.taxType) {
      final _totalAmount = max(0.0, this.totalAmount);
      return _totalAmount;
    }
    return 0;
  }

  num get salesAmount {
    this.taxType ??= 1; // 預設應稅
    this.taxRate ??= 0.0;
    if (1 == this.taxType) {
      final _totalAmount = max(0.0, this.totalAmount);
      return _totalAmount / (1.0 + this.taxRate);
    }
    return 0.0;
  }

  num get taxAmount {
    // this.taxRate ??= 0.0;
    // return this.totalAmount * this.taxRate;
    if (1 == this.taxType) {
      // final value1 = this.totalAmount;
      // final value2 = this.salesAmount;
      return max(0.0, this.totalAmount - this.salesAmount);
    }
    return 0.0;
  }
}

extension ExtensionNum on num {
  String get currency {
    return NumberFormat("#,##0", "en_US").format(this ?? 0);
  }
}

extension ExtensionReceipt on Receipt {
  String get displayBuyerMemo {
    this.buyerMemo ??= '';
    return buyerMemo.trim();
  }

  String get displaySellerMemo {
    this.sellerMemo ??= '';
    return sellerMemo.trim();
  }

  String get displayCategory {
    switch (this.category) {
      case 0:
        return '用餐';
      case 1:
        return '取貨';
      default:
        return '';
    }
  }

  num get category {
    switch (this.type) {
      case 0:
      case 1:
      case 2:
        return 0;
      case 3:
      case 4:
      case 5:
        return 1;
      default:
        return -1;
    }
  }

  String get displayType {
    switch (this.type) {
      case 0:
        return '內用';
      case 1:
        return 0 == this.source ? '外帶' : '自取';
      case 2:
        return '外送';
      case 3:
        return '自取';
      case 4:
        return '宅配';
      case 5:
        return '超商';
      default:
        return '';
    }
  }

  String get displayStatus {
    switch (this.status) {
      case 0:
        return '處理中';
      case 1:
        return '已確認';
      case 2:
        return '訂單完成';
      case 3:
        return '訂單取消';
      case 4:
        return '訂單異常';
      case 5:
        return '訂單退貨、退款';
      case 6:
        return '訂單取消';
      default:
        return '';
    }
  }

  String get displayInvoiceStatus {
    switch (this.invoiceStatus) {
      case 0:
        return '開立';
      case 1:
        return '作廢';
      case 2:
        return '折讓';
      default:
        return '';
    }
  }

  String get displayPaymentMethodId {
    this.paymentMethodId ??= -1;
    switch (this.paymentMethodId) {
      case 1:
        return "現金";
      case 2:
        return "信用卡";
      case 3:
        return "台灣Pay";
      case 4:
        return "支付寶";
      case 5:
        return "Line Pay Money";
      case 6:
        return "iCash";
      case 7:
        return "微信支付";
      case 8:
        return "Happy Cash";
      case 20:
        return "轉帳匯款";
      case 10:
        return "Line Pay";
      case 11:
        return "悠遊卡 Easy Card";
      case 12:
        return "街口支付";
      case 13:
        return "iPass 一卡通";
      case 14:
        return "Foodpanda";
      case 15:
        return "歐付寶";
      case 16:
        return "UberEats 線上金流類型 (WEB)";
      case 17:
        return "貨到付款";
      case 18:
        return "到店付款";
      case 19:
        return "綠界信用卡";
      case 9:
        return "轉帳匯款";
      default:
        return "";
    }
  }

  String get displayCreatedAt {
    if (this.createdAt == null) {
      return '';
    }
    return DateFormat('yyyy-MM-dd HH:mm').format(this.createdAt);
  }

  String get displayCheckoutAt {
    if (this.checkoutAt == null) {
      return '';
    }
    return DateFormat('MM-dd HH:mm').format(this.checkoutAt);
  }

  String get displayPrintAt {
    if (this.printAt == null) {
      return '';
    }
    return DateFormat('MM-dd HH:mm').format(this.printAt);
  }

  num get itemCount {
    this.rItems ??= [];
    return this.rItems.length;
  }

  String get displayOtherFee {
    this.otherFee ??= 0.0;
    return this.otherFee.round().currency;
  }

  String get displayDeliveryFee {
    this.deliveryFee ??= 0.0;
    return this.deliveryFee.round().currency;
  }

  String get displayServiceFee {
    this.serviceFee ??= 0.0;
    return this.serviceFee.round().currency;
  }

  String get displayPaid {
    this.paid ??= 0.0;
    return this.paid.round().currency;
  }

  String get displayChange {
    this.change ??= 0.0;
    return this.change.round().currency;
  }

  String get displayTotal {
    this.total ??= 0.0;
    return this.total.round().currency;
  }

  String get displayDiscount {
    this.discount ??= 0.0;
    return this.discount.round().currency;
  }

  String get displaySubtotal {
    this.subtotal ??= 0.0;
    return this.subtotal.round().currency;
  }

  String get displayMealAt {
    if (this.mealAt == null) {
      return '';
    }
    return DateFormat('yyyy-MM-dd HH:mm').format(this.mealAt);
  }

  String get displayPaymentStatus {
    switch (this.paymentStatus) {
      case 0:
        return '未付款';
      case 1:
        return '未結清';
      case 2:
        return '已付款';
      case 3:
        return '付款失敗';
      case 4:
        return '超過付款時間';
      default:
        return '';
    }
  }
}

extension ExtensionRItem on RItem {
  String get displayUnitPrice {
    this.unitPrice ??= 0.0;
    return this.unitPrice.round().currency;
  }

  num get totalAmount {
    this.quantity ??= 1; // 預設 1
    this.unitPrice ??= 0.0;
    return this.quantity * this.unitPrice;
  }

  String get displayTotal {
    return this.totalAmount.round().currency;
  }

  String get leftString {
    this.quantity ??= 1; // 預設 1
    return '  \$${this.displayUnitPrice} x ${this.quantity}';
  }
}
