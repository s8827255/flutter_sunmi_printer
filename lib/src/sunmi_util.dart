import 'dart:convert';

import 'package:encrypt/encrypt.dart';

import '../flutter_sunmi_printer.dart';
import 'invoice.dart';
import 'receipt.dart';

class SunmiUtil {
  static final carrierId = RegExp(r'^/[\dA-Z+-\.]{7}$');
  static final npoNab = RegExp(r'^\d{3,7}$');
  static final taxId = RegExp(r'^\d{8}$');
  // 統一編號特定倍數
  static const _taxIdWeight = [1, 2, 1, 2, 1, 2, 4, 1];

  ///
  /// 列印消費明細
  ///
  static Future<void> printReceipt(Receipt receipt) async {
    await SunmiPrinter.text(
      // '洄瀾禮好有限公司',
      receipt.storeName,
      styles: SunmiStyles(
        size: SunmiSize.title,
        align: SunmiAlign.center,
      ),
    );
    // await printText('列印時間：03-17 17:30 小明');
    await printText('列印時間：${receipt.displayPrintAt} ${receipt.userName}');
    await SunmiPrinter.row(
      cols: [
        SunmiCol(
          // text: '內用 168',
          text: receipt.displayType,
          width: 5,
          align: SunmiAlign.left,
        ),
        SunmiCol(
          // text: '訂單退貨、退款',
          text: receipt.displayStatus,
          width: 7,
          align: SunmiAlign.right,
        ),
      ],
      textSize: SunmiSize(30),
      reverse: true,
    );
    await SunmiPrinter.emptyLines(1);
    await printHeader('消費明細', '*');
    await SunmiPrinter.hr();
    // await printText('主訂單編號：line2021098765');
    await printText('訂單編號：${receipt.orderNumber}');
    // await printText('結帳時間：03-17 18:20');
    await printText('結帳時間：${receipt.displayCheckoutAt}');
    // await printText('發票號碼：QM-55560938已作廢');
    if (receipt.invoiceNumber?.isNotEmpty ?? false) {
      await printText(
          '發票號碼：${receipt.invoiceNumber} ${receipt.displayInvoiceStatus}');
    }
    // await printText('統一編號：12344321');
    if (receipt.vatNumber?.isNotEmpty ?? false) {
      await printText('統一編號：${receipt.vatNumber}');
    }
    if (receipt.carrierId?.isNotEmpty ?? false) {
      await printText('手機條碼載具：${receipt.carrierId}');
    }
    if (receipt.npoBan?.isNotEmpty ?? false) {
      await printText('愛心碼：${receipt.npoBan}');
    }
    // await printText('支付方式：-');
    await printText('支付方式：${receipt.displayPaymentMethodId}');
    await SunmiPrinter.hr();
    // await printText('合計 1訂單，3項，566元');
    await printText(
        '合計 ${receipt.billCount}訂單，${receipt.itemCount}項，${receipt.displaySubtotal}元');
    await SunmiPrinter.hr();
    // await printLeftRight('商品小計：566', '運費：0');
    switch (receipt.category) {
      case 0:
        await printLeftRight('商品小計：${receipt.displaySubtotal}',
            '服務費：${receipt.displayServiceFee}');
        break;
      case 1:
        await printLeftRight('商品小計：${receipt.displaySubtotal}',
            '運費：${receipt.displayDeliveryFee}');
        break;
      default:
    }
    // await printLeftRight('現場折價：0', '額外費用：0');
    await printLeftRight(
        '現場折價：${receipt.displayDiscount}', '額外費用：${receipt.displayOtherFee}');
    await SunmiPrinter.hr();
    await SunmiPrinter.text(
      // '商品總價：620元',
      '商品總價：${receipt.displayTotal}元',
      styles: SunmiStyles(
        size: SunmiSize.lg,
        bold: true,
      ),
    );
    // await printLeftRight('實收：620', '找零：0');
    await printLeftRight(
        '實收：${receipt.displayPaid}', '找零：${receipt.displayChange}');
    await SunmiPrinter.hr();
    // await printText('訂單編號：line2021098765');
    await printText('訂單編號：${receipt.orderNumber ?? ''}');
    // await printText('訂單時間：2021-03-17 15:20');
    await printText('訂單時間：${receipt.displayCreatedAt}');
    // await printText('取貨方式：自取');
    await printText('${receipt.displayCategory}方式：${receipt.displayType}');
    // await printText('付款方式：轉帳匯款');
    await printText('付款方式：${receipt.paymentName ?? ''}');
    // await printText('付款狀態：未付款');
    await printText('付款狀態：${receipt.displayPaymentStatus}');
    // await printText('取貨時間：2021-06-23 15:30');
    await printText('${receipt.displayCategory}時間：${receipt.displayMealAt}');
    // await printText('取貨人姓名：釋金城');
    await printText('顧客姓名：${receipt.buyerName ?? ''}');
    // await printText('取貨人電話：0912345678');
    await printText('顧客電話：${receipt.buyerPhone ?? ''}');
    await printText('顧客地址：${receipt.buyerAddress ?? ''}');
    // await printText('顧客備註：不需要塑膠袋裝。');
    await printText('顧客備註：${receipt.displayBuyerMemo ?? ''}');
    // await printText('店家備註：-');
    // 取消列印店家備註，不能讓消費者看到壞話
    // await printText('店家備註：${receipt.displaySellerMemo ?? ''}');
    await SunmiPrinter.hr();
    await printCenter('商品明細');
    await SunmiPrinter.hr();
    for (RItem item in receipt.rItems) {
      // await printText('黃金泡菜(3入裝)');
      await printText(item.itemName);
      // await printText('•2倍泡菜 •不辣');
      if (item.comment?.isNotEmpty ?? false) {
        await printText(item.comment);
      }
      // await printLeftRight('  \$1,080 x 1', '1,080元');
      await printLeftRight(item.leftString, '${item.displayTotal}元');
    }
    await SunmiPrinter.hr();
    await SunmiPrinter.text(
      // '小計：2項 金額：1,280元',
      '小計：${receipt.itemCount}項 金額：${receipt.displayTotal}元',
      styles: SunmiStyles(
        align: SunmiAlign.right,
        size: SunmiSize.def,
      ),
    );
    await SunmiPrinter.print3Line();
  }

  ///
  /// 列印折讓單
  ///
  static Future<void> printDiscount(Invoice invoice) async {
    await SunmiPrinter.text(
      // '洄瀾禮好有限公司',
      invoice.storeName,
      styles: SunmiStyles(
        size: SunmiSize.title,
        align: SunmiAlign.center,
      ),
    );
    await printCenter('營業人銷貨退回、進貨退出或');
    await printCenter('折讓證明單');
    // await printCenter('2017-12-01');
    await printCenter(invoice.displayDate);
    // await printText('賣方統編：83079973');
    await printText('賣方統編：${invoice.seller}');
    // await printText('賣方名稱：洄瀾禮好有限公司');
    await printText('賣方名稱：${invoice.storeName}');
    // await printText('發票開立日期：2017-12-01');
    await printText('發票開立日期：${invoice.displayDate}');
    // await printText('AB-00000010');
    await printText('${invoice.displayInvoiceNumber}');
    // await printText('買方統編：12656354');
    await printText('買方統編：${invoice.buyer}');
    // await printText('買方名稱：金財通商務科技服務股份有限公司');
    await printText('買方名稱：${invoice.buyerName}');
    await SunmiPrinter.emptyLines(1);
    await printHeader('銷貨明細表');
    // await printText('商品一批 \$1,000 * 1');
    // await printText('1,000 TX');
    for (var item in invoice.items) {
      await printText(item.itemName);
      await printLeftRight(item.leftString, item.rightString);
    }
    // await printLeftRight('合計：', '1項');
    await printLeftRight('合計：', invoice.displayItemsCount);
    // await printLeftRight('銷售額：', '952元');
    await printLeftRight('銷售額：', invoice.displayItemsSalesAmount);
    // await printLeftRight('稅額：', '48元');
    await printLeftRight('稅額：', invoice.displayItemsTaxAmount);
    // await printLeftRight('總計：', '1,000元');
    await printLeftRight('總計：', invoice.displayItemsTotalAmount);
    await SunmiPrinter.hr();
    await printText('簽收人：');
    await SunmiPrinter.print3Line();
  }

  ///
  /// 列印發票
  ///
  static Future<void> printInvoice(Invoice invoice) async {
    await SunmiPrinter.text(
      // '全家就是你家',
      invoice.storeName ?? '',
      styles: SunmiStyles(
        size: SunmiSize.title,
        align: SunmiAlign.center,
      ),
    );
    await SunmiPrinter.text(
      // '電子發票證明聯',
      invoice.displayPrintMark,
      styles: SunmiStyles(
        size: invoice.printMarkSize,
        align: SunmiAlign.center,
      ),
    );
    await SunmiPrinter.text(
      // '110年05-06月',
      invoice.displayTwDateTime,
      styles: SunmiStyles(
        bold: true,
        size: SunmiSize(54),
        align: SunmiAlign.center,
      ),
    );
    await SunmiPrinter.text(
      // 'NS-00000334',
      invoice.displayInvoiceNumber,
      styles: SunmiStyles(
        bold: true,
        size: SunmiSize(54),
        align: SunmiAlign.center,
      ),
    );
    await SunmiPrinter.setAlign(SunmiAlign.left);
    // SunmiPrinter.text(
    //   //'2021/05/12 18:42:40',
    //   invoice.displayDateTime,
    //   styles: SunmiStyles(
    //     bold: false,
    //     size: SunmiSize(30),
    //     align: SunmiAlign.left,
    //   ),
    // );
    await SunmiPrinter.row(
      cols: [
        SunmiCol(
          text: invoice.displayDateTime,
          width: 9,
          align: SunmiAlign.left,
        ),
        SunmiCol(
          text: invoice.displayTaxType,
          width: 3,
          align: SunmiAlign.right,
        ),
      ],
      textSize: SunmiSize.def,
    );
    await SunmiPrinter.row(
      cols: [
        SunmiCol(
          // text: '隨機碼7311',
          text: invoice.displayRandomNumber,
          width: 5,
          align: SunmiAlign.left,
        ),
        SunmiCol(
          // text: '總計9,993,782',
          text: invoice.displayTotalAmount,
          width: 7,
          align: SunmiAlign.right,
        ),
      ],
      textSize: SunmiSize.def,
    );
    await printLeftRight(invoice.displaySeller, invoice.displayBuyer);
    await SunmiPrinter.setAlign(SunmiAlign.center);
    await SunmiPrinter.printBarCode(
      // data: '10906KF427741079473',
      data: invoice.barcode,
      symbology: BarCodeSymbology.CODE39,
      height: 70,
      width: 1,
      textposition: TextPosition.NONE,
    );
    await SunmiPrinter.emptyLines(1);
    await SunmiPrinter.printDoubleQrCode(
      // code1:
      //     'AB112233441020523999900000145000001540000000001234567ydXZt4LAN1UHN/j1juVcRA==:**********:3:3:2:5Lm+6Zu75rGg'
      //         .padRight(128, ' '),
      // code2:
      //     '**OjE6MTA1OuWPo+e9qToxOjIxMDrniZvlpbY6MToyNQ=='.padRight(128, ' '),
      code1: invoice.leftQrString,
      code2: invoice.rightQrString,
      modulesize: 4,
      errorlevel: ErrorLevel.L,
    );
    await SunmiPrinter.emptyLines(1);
    await SunmiUtil.printItems(invoice);
  }

  static Future<void> printItems(Invoice invoice) async {
    await printHeader('銷貨明細表');
    for (final item in invoice.items) {
      await printText(item.itemName);
      if (item.comment?.isNotEmpty ?? false) {
        await printText(item.comment);
      }
      await printLeftRight(item.leftString, item.rightString);
    }
    // await printLeftRight('合計', '1項');
    await printLeftRight('合計', invoice.displayItemsCount);
    // await printLeftRight('銷售額', '952元');
    if (invoice.isTaxTypeMix) {
      await printLeftRight('應稅銷售額', invoice.displayItemsTXSalesAmount);
      await printLeftRight('免稅銷售額', invoice.displayItemsFreeSalesAmount);
    } else {
      await printLeftRight('銷售額', invoice.displayItemsSalesAmount);
    }
    if (invoice.showTax) {
      // await printLeftRight('稅額', '48元');
      await printLeftRight('稅額', invoice.displayItemsTaxAmount);
    }
    // await printLeftRight('總計', '1,000元');
    await printLeftRight('總計', invoice.displayItemsTotalAmount);

    await SunmiPrinter.hr();
    if (invoice.showTax) {
      // await printLeftRight('總計', '1,000元');
      await printLeftRight('總計', invoice.displayItemsTotalAmount);
      await SunmiPrinter.hr();
    }

    await SunmiPrinter.emptyLines(3);
  }

  static Future<void> printLeftRight(String lhs, String rhs,
      [bool reverse]) async {
    await SunmiPrinter.row(
      cols: [
        SunmiCol(
          // text: '買方83193989',
          text: lhs ?? '',
          width: 6,
          align: SunmiAlign.left,
        ),
        SunmiCol(
          // text: '賣方12345678',
          text: rhs ?? '',
          width: 6,
          align: SunmiAlign.right,
        ),
      ],
      textSize: SunmiSize.def,
      reverse: reverse ?? false,
    );
  }

  static Future<void> printHeader(String value, [String ch = '-']) async {
    final line = List.filled(6, ch).join();
    await SunmiPrinter.row(
      textSize: SunmiSize.def,
      cols: [
        SunmiCol(
          text: line,
          align: SunmiAlign.left,
          width: 3,
        ),
        SunmiCol(
          // text: '消費明細',
          text: value ?? '',
          align: SunmiAlign.center,
          width: 6,
        ),
        SunmiCol(
          text: line,
          align: SunmiAlign.right,
          width: 3,
        ),
      ],
    );
  }

  static Future<void> printCenter(String text) async {
    await SunmiPrinter.text(
      text,
      styles: SunmiStyles(
        size: SunmiSize.def,
        align: SunmiAlign.center,
      ),
    );
  }

  static Future<void> printRight(String text) async {
    await SunmiPrinter.text(
      text,
      styles: SunmiStyles(
        size: SunmiSize.def,
        align: SunmiAlign.right,
      ),
    );
  }

  static Future<void> printText(String text) async {
    await SunmiPrinter.text(
      text,
      styles: SunmiStyles(
        size: SunmiSize.def,
      ),
    );
  }

  static String getEncryptedString({
    String aesKey,
    String invoiceNumber,
    String randomNumber,
  }) {
    final plainText = '$invoiceNumber$randomNumber';
    final key = Key.fromBase16(aesKey);
    final iv = IV.fromBase64(kIv);
    final encrypter = Encrypter(AES(key, mode: AESMode.cbc));
    final encrypted = encrypter.encrypt(plainText, iv: iv);
    // final result = encrypter.decrypt(encrypted, iv: iv);
    return encrypted.base64;
  }

  static String getBase64String(String value) {
    final bytes = utf8.encode(value ?? "");
    return base64Encode(bytes);
  }

  /// 驗證載具
  static bool isCarrierId(String value) {
    return carrierId.hasMatch(value ?? "");
  }

  /// 驗證愛心碼
  static bool isNpoBan(String value) {
    return npoNab.hasMatch(value ?? "");
  }

  /// 驗證統一編號
  /// http://www.skrnet.com/skrjs/demo/js0161.htm
  /// https://dotblogs.com.tw/ChentingW/2020/03/29/000036
  /// http://superlevin.ifengyuan.tw/%E7%87%9F%E5%88%A9%E4%BA%8B%E6%A5%AD%E7%B5%B1%E4%B8%80%E7%B7%A8%E8%99%9F%E9%82%8F%E8%BC%AF%E6%AA%A2%E6%9F%A5%E6%96%B9%E6%B3%95/
  static bool isTaxId(String input) {
    input ??= '';
    // 驗證內容為 8 碼數字
    if (taxId.hasMatch(input) == false) {
      return false;
    }
    num sum = 0;
    final list = input.split('');
    for (var i = 0; i < list.length; i++) {
      final data = list.elementAt(i);
      // 個別乘上特定倍數,
      final subsum = num.parse(data) * _taxIdWeight.elementAt(i);
      // 若乘出來的值為二位數則將十位數和個位數相加
      sum += (subsum ~/ 10) + (subsum % 10);
    }
    if (0 == sum % 10) {
      return true;
    }
    // 若第7碼為 7, 再加上 1 被 10 整除也為正確
    if (7 == num.parse(list.elementAt(6))) {
      sum += 1;
      return 0 == sum % 10;
    }
    return false;
  }
}
