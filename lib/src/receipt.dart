// To parse this JSON data, do
//
//     final receipt = receiptFromJson(jsonString);

import 'dart:convert';

class Receipt {
  Receipt({
    this.storeName,
    this.printAt,
    this.userName,
    this.type,
    this.source,
    this.status,
    this.orderNumber,
    this.checkoutAt,
    this.invoiceNumber,
    this.invoiceStatus,
    this.vatNumber,
    this.npoBan,
    this.carrierId,
    this.mealAt,
    this.createdAt,
    this.sellerMemo,
    this.buyerName,
    this.buyerPhone,
    this.buyerMemo,
    this.buyerAddress,
    this.paymentMethodId,
    this.paid,
    this.change,
    this.total,
    this.subtotal,
    this.discount,
    this.otherFee,
    this.serviceFee,
    this.deliveryFee,
    this.paymentStatus,
    this.paymentName,
    this.billCount,
    this.rItems,
  });

  String storeName;
  DateTime printAt;
  String userName;
  num type;
  num source;
  num status;
  String orderNumber;
  DateTime checkoutAt;
  String invoiceNumber;
  num invoiceStatus;
  String vatNumber;
  String npoBan;
  String carrierId;
  DateTime mealAt;
  DateTime createdAt;
  String sellerMemo;
  String buyerName;
  String buyerPhone;
  String buyerMemo;
  String buyerAddress;
  num paymentMethodId;
  num paid;
  num change;
  num total;
  num subtotal;
  num discount;
  num otherFee;
  num serviceFee;
  num deliveryFee;
  num paymentStatus;
  String paymentName;
  num billCount;
  List<RItem> rItems;

  Receipt copyWith({
    String storeName,
    DateTime printAt,
    String userName,
    num type,
    num source,
    num status,
    String orderNumber,
    DateTime checkoutAt,
    String invoiceNumber,
    num invoiceStatus,
    String vatNumber,
    String npoBan,
    String carrierId,
    DateTime mealAt,
    DateTime createdAt,
    String sellerMemo,
    String buyerName,
    String buyerPhone,
    String buyerMemo,
    String buyerAddress,
    num paymentMethodId,
    num paid,
    num change,
    num total,
    num subtotal,
    num discount,
    num otherFee,
    num serviceFee,
    num deliveryFee,
    num paymentStatus,
    String paymentName,
    num billCount,
    List<RItem> rItems,
  }) =>
      Receipt(
        storeName: storeName ?? this.storeName,
        printAt: printAt ?? this.printAt,
        userName: userName ?? this.userName,
        type: type ?? this.type,
        source: source ?? this.source,
        status: status ?? this.status,
        orderNumber: orderNumber ?? this.orderNumber,
        checkoutAt: checkoutAt ?? this.checkoutAt,
        invoiceNumber: invoiceNumber ?? this.invoiceNumber,
        invoiceStatus: invoiceStatus ?? this.invoiceStatus,
        vatNumber: vatNumber ?? this.vatNumber,
        npoBan: npoBan ?? this.npoBan,
        carrierId: carrierId ?? this.carrierId,
        mealAt: mealAt ?? this.mealAt,
        createdAt: createdAt ?? this.createdAt,
        sellerMemo: sellerMemo ?? this.sellerMemo,
        buyerName: buyerName ?? this.buyerName,
        buyerPhone: buyerPhone ?? this.buyerPhone,
        buyerMemo: buyerMemo ?? this.buyerMemo,
        buyerAddress: buyerAddress ?? this.buyerAddress,
        paymentMethodId: paymentMethodId ?? this.paymentMethodId,
        paid: paid ?? this.paid,
        change: change ?? this.change,
        total: total ?? this.total,
        subtotal: subtotal ?? this.subtotal,
        discount: discount ?? this.discount,
        otherFee: otherFee ?? this.otherFee,
        serviceFee: serviceFee ?? this.serviceFee,
        deliveryFee: deliveryFee ?? this.deliveryFee,
        paymentStatus: paymentStatus ?? this.paymentStatus,
        paymentName: paymentName ?? this.paymentName,
        billCount: billCount ?? this.billCount,
        rItems: rItems ?? this.rItems,
      );

  factory Receipt.fromRawJson(String str) => Receipt.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Receipt.fromJson(Map<String, dynamic> json) => Receipt(
        storeName: json["store_name"] == null ? null : json["store_name"],
        printAt:
            json["print_at"] == null ? null : DateTime.parse(json["print_at"]),
        userName: json["user_name"] == null ? null : json["user_name"],
        type: json["type"] == null ? null : json["type"],
        source: json["source"] == null ? null : json["source"],
        status: json["status"] == null ? null : json["status"],
        orderNumber: json["order_number"] == null ? null : json["order_number"],
        checkoutAt: json["checkout_at"] == null
            ? null
            : DateTime.parse(json["checkout_at"]),
        invoiceNumber:
            json["invoice_number"] == null ? null : json["invoice_number"],
        invoiceStatus:
            json["invoice_status"] == null ? null : json["invoice_status"],
        vatNumber: json["vat_number"] == null ? null : json["vat_number"],
        npoBan: json["npo_ban"] == null ? null : json["npo_ban"],
        carrierId: json["carrier_id"] == null ? null : json["carrier_id"],
        mealAt:
            json["meal_at"] == null ? null : DateTime.parse(json["meal_at"]),
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        sellerMemo: json["seller_memo"] == null ? null : json["seller_memo"],
        buyerName: json["buyer_name"] == null ? null : json["buyer_name"],
        buyerPhone: json["buyer_phone"] == null ? null : json["buyer_phone"],
        buyerMemo: json["buyer_memo"] == null ? null : json["buyer_memo"],
        buyerAddress:
            json["buyer_address"] == null ? null : json["buyer_address"],
        paymentMethodId: json["payment_method_id"] == null
            ? null
            : json["payment_method_id"],
        paid: json["paid"] == null ? null : json["paid"].toDouble(),
        change: json["change"] == null ? null : json["change"].toDouble(),
        total: json["total"] == null ? null : json["total"].toDouble(),
        subtotal: json["subtotal"] == null ? null : json["subtotal"].toDouble(),
        discount: json["discount"] == null ? null : json["discount"].toDouble(),
        otherFee:
            json["other_fee"] == null ? null : json["other_fee"].toDouble(),
        serviceFee:
            json["service_fee"] == null ? null : json["service_fee"].toDouble(),
        deliveryFee: json["delivery_fee"] == null
            ? null
            : json["delivery_fee"].toDouble(),
        paymentStatus:
            json["payment_status"] == null ? null : json["payment_status"],
        paymentName: json["payment_name"] == null ? null : json["payment_name"],
        billCount: json["bill_count"] == null ? null : json["bill_count"],
        rItems: json["r_items"] == null
            ? null
            : List<RItem>.from(json["r_items"].map((x) => RItem.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "store_name": storeName == null ? null : storeName,
        "print_at": printAt == null ? null : printAt.toIso8601String(),
        "user_name": userName == null ? null : userName,
        "type": type == null ? null : type,
        "source": source == null ? null : source,
        "status": status == null ? null : status,
        "order_number": orderNumber == null ? null : orderNumber,
        "checkout_at": checkoutAt == null ? null : checkoutAt.toIso8601String(),
        "invoice_number": invoiceNumber == null ? null : invoiceNumber,
        "invoice_status": invoiceStatus == null ? null : invoiceStatus,
        "vat_number": vatNumber == null ? null : vatNumber,
        "npo_ban": npoBan == null ? null : npoBan,
        "carrier_id": carrierId == null ? null : carrierId,
        "meal_at": mealAt == null ? null : mealAt.toIso8601String(),
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "seller_memo": sellerMemo == null ? null : sellerMemo,
        "buyer_name": buyerName == null ? null : buyerName,
        "buyer_phone": buyerPhone == null ? null : buyerPhone,
        "buyer_memo": buyerMemo == null ? null : buyerMemo,
        "buyer_address": buyerAddress == null ? null : buyerAddress,
        "payment_method_id": paymentMethodId == null ? null : paymentMethodId,
        "paid": paid == null ? null : paid,
        "change": change == null ? null : change,
        "total": total == null ? null : total,
        "subtotal": subtotal == null ? null : subtotal,
        "discount": discount == null ? null : discount,
        "other_fee": otherFee == null ? null : otherFee,
        "service_fee": serviceFee == null ? null : serviceFee,
        "delivery_fee": deliveryFee == null ? null : deliveryFee,
        "payment_status": paymentStatus == null ? null : paymentStatus,
        "payment_name": paymentName == null ? null : paymentName,
        "bill_count": billCount == null ? null : billCount,
        "r_items": rItems == null
            ? null
            : List<dynamic>.from(rItems.map((x) => x.toJson())),
      };
}

class RItem {
  RItem({
    this.itemName,
    this.quantity,
    this.unitPrice,
    this.comment,
  });

  String itemName;
  num quantity;
  num unitPrice;
  String comment;

  RItem copyWith({
    String itemName,
    num quantity,
    num unitPrice,
    String comment,
  }) =>
      RItem(
        itemName: itemName ?? this.itemName,
        quantity: quantity ?? this.quantity,
        unitPrice: unitPrice ?? this.unitPrice,
        comment: comment ?? this.comment,
      );

  factory RItem.fromRawJson(String str) => RItem.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory RItem.fromJson(Map<String, dynamic> json) => RItem(
        itemName: json["item_name"] == null ? null : json["item_name"],
        quantity: json["quantity"] == null ? null : json["quantity"],
        unitPrice:
            json["unit_price"] == null ? null : json["unit_price"].toDouble(),
        comment: json["comment"] == null ? null : json["comment"],
      );

  Map<String, dynamic> toJson() => {
        "item_name": itemName == null ? null : itemName,
        "quantity": quantity == null ? null : quantity,
        "unit_price": unitPrice == null ? null : unitPrice,
        "comment": comment == null ? null : comment,
      };
}
