/*
 * flutter_sunmi_printer
 * Created by Andrey U.
 * 
 * Copyright (c) 2020. All rights reserved.
 * See LICENSE for distribution and usage details.
 */

import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';
import 'package:flutter/services.dart';
import 'package:flutter_sunmi_printer/src/enums.dart';
import 'sunmi_col.dart';
import 'sunmi_styles.dart';

class SunmiPrinter {
  static const String RESET = "reset";
  // static const String START_PRINT = "startPrint";
  // static const String STOP_PRINT = "stopPrint";
  // static const String IS_PRINTING = "isPrinting";
  static const String BOLD_ON = "boldOn";
  static const String BOLD_OFF = "boldOff";
  static const String UNDERLINE_ON = "underlineOn";
  static const String UNDERLINE_OFF = "underlineOff";
  static const String EMPTY_LINES = "emptyLines";
  static const String PRINT_TEXT = "printText";
  static const String PRINT_ROW = "printRow";
  static const String PRINT_IMAGE = "printImage";
  static const String FEED_PAPER = "feedPaper";
  static const String PRINT_ORIGIN_TEXT = "printOriginalText";
  static const String PRINT_TEXT_2 = "printText2";
  static const String PRINT_TABLE_ITEM = "printTableItem";
  static const String SET_FONT_SIZE = "setFontSize";
  static const String LINE_WRAP = "lineWrap";
  static const String PRINT_TABLE = "printTable";
  static const String PRINT_QR_CODE = "printQrCode";
  static const String PRINT_BAR_CODE = "printBarCode";
  static const String SET_ALIGN = "setAlign";
  static const String PRINT_3_LINE = "print3Line";
  static const String PRINT_DOUBLE_QR_CODE = "printDoubleQrCode";
  static const String SEND_RAW_DATA = "sendRawData";
  static const String SET_PRINTER_STYLE = "setPrinterStyle";

  static const MethodChannel _channel =
      const MethodChannel('flutter_sunmi_printer');

  static Future<void> reset() async {
    await _channel.invokeMethod(RESET);
  }

  static Future<void> feedPaper() async {
    await _channel.invokeMethod(FEED_PAPER);
  }

  // static Future<void> startPrint() async {
  //   await _channel.invokeMethod(START_PRINT);
  // }

  // static Future<void> stopPrint() async {
  //   await _channel.invokeMethod(STOP_PRINT);
  // }

  // static Future<void> isPrinting() async {
  //   await _channel.invokeMethod(IS_PRINTING);
  // }

  /// Print [text] with [styles] and skip [linesAfter] after
  static Future<void> text(
    String text, {
    SunmiStyles styles = const SunmiStyles(),
    int linesAfter = 0,
  }) async {
    await _channel.invokeMethod(PRINT_TEXT, {
      "text": text ?? '',
      "bold": styles.bold,
      "underline": styles.underline,
      "align": styles.align.value,
      "size": styles.size.value,
      "linesAfter": linesAfter,
      "reverse": styles.reverse,
    });
  }

  /// Skip [n] lines
  static Future<void> emptyLines(int n) async {
    if (n > 0) {
      await _channel.invokeMethod(EMPTY_LINES, {"n": n});
    }
  }

  /// Print horizontal full width separator
  static Future<void> hr({
    String ch = '-',
    int len = 31,
    linesAfter = 0,
  }) async {
    await text(List.filled(len, ch[0]).join(), linesAfter: linesAfter);
  }

  /// Print a row.
  ///
  /// A row contains up to 12 columns. A column has a width between 1 and 12.
  /// Total width of columns in one row must be equal to 12.
  static Future<void> row({
    List<SunmiCol> cols,
    bool bold: false,
    bool underline: false,
    bool reverse: false,
    SunmiSize textSize: SunmiSize.md,
    int linesAfter: 0,
  }) async {
    final isSumValid = cols.fold(0, (int sum, col) => sum + col.width) == 12;
    if (!isSumValid) {
      throw Exception('Total columns width must be equal to 12');
    }

    final colsJson = List<Map<String, String>>.from(
        cols.map<Map<String, String>>((SunmiCol col) => col.toJson()));

    await _channel.invokeMethod(PRINT_ROW, {
      "cols": json.encode(colsJson),
      "bold": bold,
      "underline": underline,
      "textSize": textSize.value,
      "linesAfter": linesAfter,
      "reverse": reverse,
    });
  }

  static Future<void> boldOn() async {
    await _channel.invokeMethod(BOLD_ON);
  }

  static Future<void> boldOff() async {
    await _channel.invokeMethod(BOLD_OFF);
  }

  static Future<void> underlineOn() async {
    await _channel.invokeMethod(UNDERLINE_ON);
  }

  static Future<void> underlineOff() async {
    await _channel.invokeMethod(UNDERLINE_OFF);
  }

  static Future<void> image(
    String base64, {
    SunmiAlign align: SunmiAlign.center,
  }) async {
    await _channel.invokeMethod(PRINT_IMAGE, {
      "base64": base64,
      "align": align.value,
    });
  }

  static Future<void> printOriginalText(String text) async {
    await _channel.invokeMethod(
      PRINT_ORIGIN_TEXT,
      {
        "text": text,
      },
    );
  }

  // static Future<void> printTableItem(
  //     List<String> txts, List<int> width, List<int> align) async {
  //   await _channel.invokeMethod(PRINT_TABLE_ITEM, {
  //     "txts": txts,
  //     "width": width,
  //     "align": align,
  //   });
  // }

  static Future<void> setFontSize(int size) async {
    await _channel.invokeMethod(
      SET_FONT_SIZE,
      {
        "size": size,
      },
    );
  }

  // static Future<void> lineWrap(int n) async {
  //   if (n > 0) {
  //     await _channel.invokeMethod(LINE_WRAP, {
  //       "n": n,
  //     });
  //   }
  // }

  // static Future<void> printTable(
  //     List<String> txts, List<int> width, List<int> align) async {
  //   await _channel.invokeMethod(PRINT_TABLE, {
  //     "txts": txts,
  //     "width": width,
  //     "align": align,
  //   });
  // }

  /// @param modulesize: 二维码块大小(单位:点, 取值 1 至 16 )
  /// @param errorlevel: 二维码纠错等级(0 至 3)
  /// 0 -- 纠错级别L ( 7%)
  /// 1 -- 纠错级别M (15%)
  /// 2 -- 纠错级别Q (25%)
  /// 3 -- 纠错级别H (30%)
  static Future<void> printQrCode({
    String data,
    int modulesize,
    ErrorLevel errorlevel,
  }) async {
    await _channel.invokeMethod(PRINT_QR_CODE, {
      "data": data,
      "modulesize": modulesize,
      "errorlevel": errorlevel.value,
    });
  }

  /// @param symbology: 条码类型（0 - 8）
  /// 0: UPC-A
  /// 1: UPC-E
  /// 2: JAN13(EAN13)
  /// 3: JAN8(EAN8)
  /// 4: CODE39
  /// 5: ITF
  /// 6: CODABAR
  /// 7: CODE93
  /// 8: CODE128
  /// @param width: 条码宽度, 取值 2 - 6, 默认：2。
  /// @param textposition:
  /// 0: 不打印⽂字
  /// 1: ⽂字在条码上⽅
  /// 2: ⽂字在条码下⽅
  /// 3: 条码上下⽅均打印
  /// @param height: 条码⾼度, 取值 1 - 255, 默认：162
  static Future<void> printBarCode({
    String data,
    // int symbology,
    BarCodeSymbology symbology,
    int height,
    int width,
    TextPosition textposition,
  }) async {
    await _channel.invokeMethod(PRINT_BAR_CODE, {
      "data": data,
      "symbology": symbology.value,
      "height": height,
      "width": width,
      "textposition": textposition.value,
    });
  }

  static Future<void> setAlign(SunmiAlign align) async {
    await _channel.invokeMethod(SET_ALIGN, {
      "align": align.value,
    });
  }

  static Future<void> print3Line() async {
    await _channel.invokeMethod(PRINT_3_LINE);
  }

  /// 横向两个二维码 sunmi自定义指令
  ///
  /// @param code1:      二维码数据
  /// @param code2:      二维码数据
  /// @param modulesize: 二维码块大小(单位:点, 取值 1 至 16 )
  /// @param errorlevel: 二维码纠错等级(0 至 3)
  /// 0 -- 纠错级别L ( 7%)
  /// 1 -- 纠错级别M (15%)
  /// 2 -- 纠错级别Q (25%)
  /// 3 -- 纠错级别H (30%)
  static Future<void> printDoubleQrCode({
    String code1,
    String code2,
    int modulesize,
    ErrorLevel errorlevel,
  }) async {
    await _channel.invokeMethod(PRINT_DOUBLE_QR_CODE, {
      "code1": code1,
      "code2": code2,
      "modulesize": modulesize,
      "errorlevel": errorlevel.value,
    });
  }

  // static Future<void> text2(
  //   String text, {
  //   SunmiStyles styles = const SunmiStyles(),
  //   int linesAfter = 0,
  // }) async {
  //   await _channel.invokeMethod(PRINT_TEXT_2, {
  //     "content": text,
  //     "size": styles.size.value,
  //     "isBold": styles.bold,
  //     "isUnderLine": styles.underline,
  //     "reverse": styles.reverse,
  //   });
  // }

  static Future<void> setPrinterStyle({
    int key,
    int val,
  }) async {
    await _channel.invokeMethod(SET_PRINTER_STYLE, {
      "key": key,
      "val": key,
    });
  }

  static Future<void> sendRawData({
    Uint8List data,
  }) async {
    await _channel.invokeMethod(SEND_RAW_DATA, {
      "data": data,
    });
  }
}
